package com.codeboard.expensetracker.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codeboard.expensetracker.expenseDTO.ExpenseDTO;
import com.codeboard.expensetracker.model.Expense;
import com.codeboard.expensetracker.service.ExpenseService;

@RestController
@CrossOrigin("http://localhost:4200/")
@RequestMapping
public class ExpenseController {

	@Autowired
	private ExpenseService expenseService;

	@GetMapping("/expense")
	ResponseEntity<List<ExpenseDTO>> get() {
//	public ResponseEntity<List<Expense>> get() {
		return expenseService.getExpense();
	}

	@GetMapping("/expense/{id}")
	public ResponseEntity<ExpenseDTO> getExpenseById(@PathVariable("id") int id) {
		return expenseService.getById(id);
	}

	@DeleteMapping("/expense/{id}")
	public void deleteExpenseById(@PathVariable("id") int id) {
		expenseService.deleteById(id);
	}

	@PostMapping("/expense")
	public ResponseEntity<ExpenseDTO> saveExpense(@RequestBody Expense expense) {
		return expenseService.save(expense);
	}

}
