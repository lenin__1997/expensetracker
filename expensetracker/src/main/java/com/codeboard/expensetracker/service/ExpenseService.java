package com.codeboard.expensetracker.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.codeboard.expensetracker.expenseDTO.ExpenseDTO;
import com.codeboard.expensetracker.model.Expense;

public interface ExpenseService {

	ResponseEntity<List<ExpenseDTO>> getExpense();
	
	ResponseEntity<ExpenseDTO> getById(int id);

	void deleteById(int id);

	ResponseEntity<ExpenseDTO> save(Expense expense);
		
}
