package com.codeboard.expensetracker.service;

import java.util.ArrayList;
import java.util.List;
import org.apache.catalina.mbeans.MBeanUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.codeboard.expensetracker.expenseDTO.ExpenseDTO;
import com.codeboard.expensetracker.model.Expense;
import com.codeboard.expensetracker.repository.ExpenseRepository;

import jakarta.persistence.Entity;

@Service
public class ExpenseServiceImpl implements ExpenseService {
	
	@Autowired
	private ExpenseRepository expenseRepository;

	@Override
	public ResponseEntity<List<ExpenseDTO>> getExpense(){
		List<Expense> expense = expenseRepository.findAll();
		List<ExpenseDTO> expenseDto = new ArrayList<>();
		for (Expense n : expense) {
			ExpenseDTO dto = new ExpenseDTO();
			dto.setId(n.getId());
			dto.setExpense(n.getExpense());
			dto.setDescription(n.getDescription());
			dto.setAmount(n.getAmount());
			expenseDto.add(dto);		
	    }
		return new ResponseEntity<List<ExpenseDTO>>(expenseDto,HttpStatus.OK);	
	}

	@Override
	public ResponseEntity<ExpenseDTO> getById(int id) {
	Expense expense	= expenseRepository.getReferenceById(id);
	ExpenseDTO expenseDto = new ExpenseDTO();
	BeanUtils.copyProperties(expense, expenseDto);
	return new ResponseEntity<ExpenseDTO>(expenseDto,HttpStatus.OK);
	}


	@Override
	public void deleteById(int id) {
		expenseRepository.deleteById(id);
	}


	@Override
	public ResponseEntity<ExpenseDTO> save(Expense expense) {
		Expense expense1 = expenseRepository.save(expense);
		ExpenseDTO expenDto = new ExpenseDTO();
		BeanUtils.copyProperties(expense1, expenDto);
		return new ResponseEntity<ExpenseDTO>(HttpStatus.OK);
	}
}
