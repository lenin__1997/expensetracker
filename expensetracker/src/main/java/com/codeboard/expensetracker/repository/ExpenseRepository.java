package com.codeboard.expensetracker.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.codeboard.expensetracker.model.Expense;


@Repository
public interface ExpenseRepository extends JpaRepository<Expense, Integer> {

}
