import { Component } from '@angular/core';
import { Expense } from '../expense';
import { HttpexpenseService } from '../httpexpense.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-add-expense',
  templateUrl: './add-expense.component.html',
  styleUrls: ['./add-expense.component.css']
})
export class AddExpenseComponent {

  expense: Expense = new Expense();

  constructor(private httpexpenseService: HttpexpenseService,
    private router:Router,private activeRoute : ActivatedRoute){}
  
  ngOnInit():void{
    const isIdPresent = this.activeRoute.snapshot.paramMap.has('id');
    if(isIdPresent){
      let id = Number(this.activeRoute.snapshot.paramMap.get('id'));
      this.httpexpenseService.editById(id).subscribe(
        data=>this.expense=data)
    }
  }

  saveExpense(){
    this.httpexpenseService.saveExpense(this.expense).subscribe(
      data=>{
        console.log("respose",data);
        this.router.navigateByUrl("/expense");
    })
  }

  deleteExpense(id:number){
    this.httpexpenseService.deleteById(id).subscribe(data=>{
      console.log("This id : "+id+" is Deleted");
      this.router.navigateByUrl('/expense');
    })
  }
}
