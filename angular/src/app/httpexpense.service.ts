import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map } from 'rxjs';
import { Expense } from './expense';

@Injectable({
  providedIn: 'root'
})
export class HttpexpenseService {


  constructor(private httpClient:HttpClient) { }

  getExpense(): Observable<Expense[]>{
    return this.httpClient.get<Expense[]>('http://localhost:8080/expense').pipe(
      map(Response=>Response)
    )
  }

  // getExpense(): Observable<Expense[]>{
  //   return this.httpClient.get<Expense[]>('http://localhost:8080/expense');
  // }

  saveExpense(expense: Expense) :Observable<Expense>{
    return this.httpClient.post<Expense>('http://localhost:8080/expense',expense);
  }

  editById(id:number) : Observable<Expense>{
    return this.httpClient.get<Expense>(`${'http://localhost:8080/expense'}/${id}`).pipe(map(Response=>Response));
  }

  deleteById(id:number) : Observable<any>{
    return this.httpClient.delete<Expense>(`${'http://localhost:8080/expense'}/${id}`);
  }

}
