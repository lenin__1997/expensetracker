import { Component } from '@angular/core';
import { HttpexpenseService } from '../httpexpense.service';
import { Expense } from '../expense';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent {

  expense: Expense[] = [];
  filter = {
    keyword:'',
    sortBy:"Name"
  }

  constructor(private httpexpenseService:HttpexpenseService,
    private router:Router){}

    // Refresh List
    // listExpenses(){
    //   this.httpexpenseService.getExpense().subscribe(Response=>
    //     this.handleSuccessfulResponse(Response));  
    //   }
    // handleSuccessfulResponse(Response: Expense[]){
    //   this.expense=this.filterExpenses(Response);
    // }

    listExpenses(){
      this.httpexpenseService.getExpense().subscribe(
        data=>this.expense= this.filterExpenses(data)
        )
    }

  ngOnInit(): void{
    this.listExpenses();
  }

  
  deleteExpense(id:number){
    this.httpexpenseService.deleteById(id).subscribe(data=>{
      console.log("This id : "+id+" is Deleted");
    })

  }



  filterExpenses(expense: Expense[]){
    return expense.filter((e)=>{
      return e.expense.toLowerCase().includes(this.filter.keyword.toLowerCase());
    }).sort((a,b) => {
      if(this.filter.sortBy ==='Name'){
        return a.expense.toLowerCase() < b.expense.toLowerCase() ? -1 : 1 ;
      }
      else{
        return a.amount > b.amount ? -1 : 1;
      }
    })
  }

}
