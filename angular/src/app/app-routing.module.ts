import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { AddExpenseComponent } from './add-expense/add-expense.component';

const routes: Routes = [
  {path:"expense",component:ListComponent},
  {path:"edit/:id",component:AddExpenseComponent},
  {path:"",component:ListComponent},
  {path:"add",component:AddExpenseComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
